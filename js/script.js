const shadowDiv = document.createElement("div");
shadowDiv.attachShadow({ mode: "open" });
const divElement = document.createElement("div");
divElement.className = "block";
divElement.innerHTML = `
<button class='block_btn-close' style='position: absolute; top: 10px; right: 10px; border-radius: 5px;'>x</button>
<h2 class='block__title' style='margin-bottom: 10px; margin-top: 0px'>Search node element</h2> 
<input class='block__info' style='margin-bottom: 15px'/>
<button class='block_btn-search' style='width: 70px; margin-left: 20px'>Search</button> 
<div class='block__btn'>
<button class='block_btn-prev-element' style='width: 70px; margin-right: 10px'>Prev</button> 
<button class='block_btn-next-element' style='width: 70px; margin-right: 10px'>Next</button> 
<button class='block_btn-parent-element' style='width: 70px; margin-right: 10px'>Parent</button> 
<button class='block_btn-child-element' style='width: 70px'>Children</button>
</div>
`;
// document.body.append(divElement);

shadowDiv.appendChild(divElement);
shadowDiv.shadowRoot.appendChild(divElement)
document.body.prepend(shadowDiv);


divElement.style.cssText = `position: fixed;
width: 325px;
top: 0;
right: 0;
background-color: #ffffff;
border-radius: 5px;
padding: 20px;
z-index: 1000;`

const btnSearch = divElement.getElementsByClassName('block_btn-search')[0];
const btnPrev = divElement.getElementsByClassName('block_btn-prev-element')[0];
const btnNext = divElement.getElementsByClassName('block_btn-next-element')[0];
const btnParent = divElement.getElementsByClassName('block_btn-parent-element')[0];
const btnChild = divElement.getElementsByClassName('block_btn-child-element')[0];
const btnClose = divElement.getElementsByClassName('block_btn-close')[0];
let searchItem;
let tempItem;
let theTarget, theTaggetClass;

btnSearch.addEventListener("click", () => {
    let valueSearch = divElement.getElementsByClassName('block__info')[0].value;
    console.log("valueSearch", valueSearch)
    searchItem = document.querySelector(valueSearch);
    if (searchItem === null){
        alert("Element not found!")
    }else{
        searchItem.style.cssText = `border: 2px solid red`;
        disableBtn(searchItem);
        searchItem.scrollIntoView({block: "center"});
    }  
})

btnPrev.addEventListener("click", (event) => {
    searchItem.style.cssText = `border: none`;
    theTarget = event.target;
    theTaggetClass = event.target.className;
    btnWork(theTaggetClass);
    console.log(searchItem);
    console.log(theTarget);
})

btnNext.addEventListener("click", (event) => {
    searchItem.style.cssText = `border: none`;
    theTarget = event.target;
    theTaggetClass = event.target.className;
    btnWork(theTaggetClass);
    console.log(searchItem);
    console.log(theTarget);
})

btnParent.addEventListener("click", (event) => {
    searchItem.style.cssText = `border: none`;
    theTarget = event.target;
    theTaggetClass = event.target.className;
    btnWork(theTaggetClass);
    console.log(searchItem);
    console.log(theTarget);
})

btnChild.addEventListener("click", (event) => {
    searchItem.style.cssText = `border: none`;
    theTarget = event.target;
    theTaggetClass = event.target.className;
    btnWork(theTaggetClass);
    console.log(searchItem);
    console.log(theTarget);
})

function btnWork(elClass){
    if ((elClass === 'block_btn-prev-element') && (searchItem.previousElementSibling)){
        tempItem = searchItem.previousElementSibling;
        tempItem.style.cssText = `border: 2px solid red`;
        tempItem.scrollIntoView({block: "center"});
        searchItem = tempItem;
        disableBtn(searchItem);
    } 
    if ((elClass === 'block_btn-next-element') && (searchItem.nextElementSibling)){
        tempItem = searchItem.nextElementSibling;
        tempItem.style.cssText = `border: 2px solid red`;
        tempItem.scrollIntoView({block: "center"});
        searchItem = tempItem;
        disableBtn(searchItem);
    } 
    if ((elClass === 'block_btn-parent-element') && (searchItem.parentElement)){
        tempItem = searchItem.parentElement;
        tempItem.style.cssText = `border: 2px solid red`;
        tempItem.scrollIntoView({block: "center"});
        searchItem = tempItem;
        disableBtn(searchItem);
    } 
    if ((elClass === 'block_btn-child-element') && (searchItem.firstElementChild)){
        tempItem = searchItem.firstElementChild;
        tempItem.style.cssText = `border: 2px solid red`;
        tempItem.scrollIntoView({block: "center"});
        searchItem = tempItem;
        disableBtn(searchItem);
    }else{
        console.log("fdfdfd")
    } 
}

function disableBtn(el){
    if(!el.previousElementSibling){
        btnPrev.setAttribute("disabled","disabled");
    }else{
        btnPrev.removeAttribute("disabled");
    }
    if(!el.nextElementSibling){
        btnNext.setAttribute("disabled","disabled");
    }else{
        btnNext.removeAttribute("disabled");
    }
    if(!el.firstElementChild){
        btnChild.setAttribute("disabled","disabled");
    }else{
        btnChild.removeAttribute("disabled");
    }
    if(!el.parentElement){
        btnParent.setAttribute("disabled","disabled");
    }else{
        btnParent.removeAttribute("disabled");
    }
}

btnClose.addEventListener("click", () => {
    divElement.remove();
    searchItem.style.cssText = `border: none`;
}) 

function moveTab(divElement) {
    let startX = 0;
    let startY = 0;
    let shiftX = 0;
    let shiftY = 0;
    divElement.onmousedown = onmousedownFunction;

    function onmousedownFunction(e) {
        shiftX = e.clientX;
        shiftY = e.clientY;
        divElement.onmouseup = closeElement;
        divElement.onmousemove = moveElement;
    }

    function moveElement(e) {
        startX = shiftX - e.clientX;
        startY = shiftY - e.clientY;
        shiftX = e.clientX;
        shiftY = e.clientY;
        divElement.style.top = (divElement.offsetTop - startY) + "px";
        divElement.style.left = (divElement.offsetLeft - startX) + "px";

        if ((divElement.offsetTop) < 0 ){
            divElement.style.top = 0 + 'px';
        }
        if((divElement.offsetLeft) < 0 ){
            divElement.style.left = 0 + 'px';
        }
    }

    function closeElement() {
        divElement.onmouseup = null;
        divElement.onmousemove = null;
        // document.removeEventListener('mousemove', onMouseMove);
    }

    divElement.ondragstart = function () {
        return false;
    };
}

moveTab(divElement)

// const shadowDiv=document.createElement("div");shadowDiv.attachShadow({mode:"open"});const divElement=document.createElement("div");divElement.className="block",divElement.innerHTML="\n<button class='block_btn-close' style='position: absolute; top: 10px; right: 10px; border-radius: 5px;'>x</button>\n<h2 class='block__title' style='margin-bottom: 10px; margin-top: 0px'>Введите значение</h2> \n<input class='block__info' style='margin-bottom: 15px'/>\n<button class='block_btn-search' style='width: 70px; margin-left: 20px'>Search</button> \n<div class='block__btn'>\n<button class='block_btn-prev-element' style='width: 70px; margin-right: 10px'>Prev</button> \n<button class='block_btn-next-element' style='width: 70px; margin-right: 10px'>Next</button> \n<button class='block_btn-parent-element' style='width: 70px; margin-right: 10px'>Parent</button> \n<button class='block_btn-child-element' style='width: 70px'>Children</button>\n</div>\n",shadowDiv.appendChild(divElement),shadowDiv.shadowRoot.appendChild(divElement),document.body.prepend(shadowDiv),divElement.style.cssText="position: fixed;\nwidth: 325px;\ntop: 0;\nright: 0;\nbackground-color: #ffffff;\nborder-radius: 5px;\npadding: 20px;\nz-index: 1000;";const btnSearch=divElement.getElementsByClassName("block_btn-search")[0],btnPrev=divElement.getElementsByClassName("block_btn-prev-element")[0],btnNext=divElement.getElementsByClassName("block_btn-next-element")[0],btnParent=divElement.getElementsByClassName("block_btn-parent-element")[0],btnChild=divElement.getElementsByClassName("block_btn-child-element")[0],btnClose=divElement.getElementsByClassName("block_btn-close")[0];let searchItem,tempItem,theTarget,theTaggetClass;function btnWork(e){if("block_btn-prev-element"===e&&searchItem.previousElementSibling)return(tempItem=searchItem.previousElementSibling).style.cssText="border: 2px solid red",tempItem.scrollIntoView({block:"center"}),disableBtn(searchItem=tempItem),searchItem;"block_btn-next-element"===e&&searchItem.nextElementSibling&&((tempItem=searchItem.nextElementSibling).style.cssText="border: 2px solid red",tempItem.scrollIntoView({block:"center"}),disableBtn(searchItem=tempItem)),"block_btn-parent-element"===e&&searchItem.parentElement&&((tempItem=searchItem.parentElement).style.cssText="border: 2px solid red",tempItem.scrollIntoView({block:"center"}),disableBtn(searchItem=tempItem)),"block_btn-child-element"===e&&searchItem.firstElementChild&&((tempItem=searchItem.firstElementChild).style.cssText="border: 2px solid red",tempItem.scrollIntoView({block:"center"}),disableBtn(searchItem=tempItem))}function disableBtn(e){e.previousElementSibling?btnPrev.removeAttribute("disabled","disabled"):btnPrev.setAttribute("disabled","disabled"),e.nextElementSibling?btnNext.removeAttribute("disabled","disabled"):btnNext.setAttribute("disabled","disabled"),e.firstElementChild?btnParent.removeAttribute("disabled","disabled"):btnParent.setAttribute("disabled","disabled"),e.parentElement?btnChild.removeAttribute("disabled","disabled"):btnChild.setAttribute("disabled","disabled")}function moveTab(e){let t=0,n=0,l=0,s=0;function o(o){t=l-o.clientX,n=s-o.clientY,l=o.clientX,s=o.clientY,e.style.top=e.offsetTop-n+"px",e.style.left=e.offsetLeft-t+"px",e.offsetTop<0&&(e.style.top="0px"),e.offsetLeft<0&&(e.style.left="0px")}function r(){e.onmouseup=null,e.onmousemove=null}e.onmousedown=function(t){l=t.clientX,s=t.clientY,e.onmouseup=r,e.onmousemove=o},e.ondragstart=function(){return!1}}btnSearch.addEventListener("click",()=>{let e=divElement.getElementsByClassName("block__info")[0].value;console.log("valueSearch",e),null===(searchItem=document.querySelector(e))?alert("Element not found!"):(searchItem.style.cssText="border: 2px solid red",disableBtn(searchItem),searchItem.scrollIntoView({block:"center"}))}),btnPrev.addEventListener("click",e=>{searchItem.style.cssText="border: none",theTarget=e.target,btnWork(theTaggetClass=e.target.className)}),btnNext.addEventListener("click",e=>{searchItem.style.cssText="border: none",theTarget=e.target,btnWork(theTaggetClass=e.target.className)}),btnParent.addEventListener("click",e=>{searchItem.style.cssText="border: none",theTarget=e.target,btnWork(theTaggetClass=e.target.className)}),btnChild.addEventListener("click",e=>{searchItem.style.cssText="border: none",theTarget=e.target,btnWork(theTaggetClass=e.target.className)}),btnClose.addEventListener("click",()=>{divElement.remove(),searchItem.style.cssText="border: none"}),moveTab(divElement);